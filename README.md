# pickr

Android application to view Flickr photos.

<img src="https://github.com/fabienbarbero/pickr/raw/master/screenshots/gallery.jpg" alt="Gallery" width="367" height="569">
<img src="https://github.com/fabienbarbero/pickr/raw/master/screenshots/photoset.jpg" alt="Gallery" width="367" height="569">
