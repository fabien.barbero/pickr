/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.PhotoUploader;
import com.github.fabienbarbero.pickr.utils.Logger;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public abstract class ThumbsGridFragment
        extends FlickrFragment
        implements OnItemClickListener, View.OnClickListener, Handler.Callback, SwipeRefreshLayout.OnRefreshListener
{

    private static final Logger LOGGER = Logger.getLogger( ThumbsGridFragment.class );
    private static final int REQUEST_IMAGE_CAPTURE = 25;
    protected final Handler mHandler;

    protected GridView mGridView;
    protected View mThrobber;
    protected FloatingActionButton mTakePhotoButton;
    private File mPhotoFile;
    protected SwipeRefreshLayout mRefreshLayout;

    protected ThumbsGridFragment( int titleId )
    {
        super( titleId );
        mHandler = new Handler( this );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_thumbs_grid, container, false );
        mGridView = view.findViewById( R.id.photosThumbsGridView );
        mThrobber = view.findViewById( R.id.photosThumbsGridProgressBar );
        mTakePhotoButton = view.findViewById( R.id.takePhotoButton );

        mRefreshLayout = view.findViewById( R.id.swipeRefresh );
        mRefreshLayout.setColorSchemeColors( getResources().getColor( R.color.primary_color ) );
        mRefreshLayout.setOnRefreshListener( this );

        if ( !mIsMe ) {
            mTakePhotoButton.setVisibility( View.GONE );
        }
        mGridView.setOnItemClickListener( this );
        mTakePhotoButton.setOnClickListener( this );

        return view;
    }

    @Override
    public void onRefresh()
    {
        reload();
    }

    @Override
    public void onClick( View v )
    {
        switch ( v.getId() ) {
            case R.id.takePhotoButton:
                try {
                    File storageDir = getActivity().getExternalFilesDir( Environment.DIRECTORY_PICTURES );
                    mPhotoFile = File.createTempFile( "snapshot-", ".jpg", storageDir ).getCanonicalFile();
                    Uri photoURI = FileProvider.getUriForFile( getActivity(),
                                                               "com.github.fabienbarbero.pickr.fileprovider",
                                                               mPhotoFile );

                    Intent takePictureIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
                    takePictureIntent.putExtra( MediaStore.EXTRA_OUTPUT, photoURI );
                    startActivityForResult( takePictureIntent, REQUEST_IMAGE_CAPTURE );

                } catch ( IOException ex ) {
                    LOGGER.error( "Error configuring snapshot", ex );
                }
                break;
        }
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if ( requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK ) {
            new PhotoUploader( mPhotoFile, mHandler ).execute();
        }
    }

    protected abstract void reload();

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case PhotoUploader.PHOTO_ADDED:
                reload();
                break;
        }
        return false;
    }

    protected void onPhotoTaken()
    {

    }

}
