/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;

import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.Photoset;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.HeaderImageLoader;
import com.github.fabienbarbero.pickr.tasks.PhotosetPhotosLoader;
import com.github.fabienbarbero.pickr.ui.adapters.PhotosThumbsAdapter;

public class PhotosetActivity
        extends AppCompatActivity
        implements Callback, OnItemClickListener
{

    public static final String PARAM_SET = "set";

    private Handler mHandler;
    private GridView mGridView;
    private View mThrobber;
    private PhotosThumbsAdapter mAdapter;
    private Paginated<Photo> mPhotos;
    private Photoset mSet;
    private ImageView mImageView;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_photoset );
        mHandler = new Handler( this );

        mGridView = findViewById( R.id.photosetGridView );
        mThrobber = findViewById( R.id.photosetProgressBar );
        mImageView = findViewById( R.id.photosetMainImageView );

        mGridView.setOnItemClickListener( this );

        mSet = (Photoset) getIntent().getSerializableExtra( PARAM_SET );

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle( mSet.getTitle() );
        actionBar.setDisplayHomeAsUpEnabled( true );
        actionBar.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );

        mThrobber.setVisibility( View.VISIBLE );
        new PhotosetPhotosLoader( mSet, mHandler ).execute();
        new HeaderImageLoader( mSet.getPrimaryPhoto(), mImageView ).execute();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if ( mPhotos != null ) {
            for ( Photo photo : mPhotos ) {
                BitmapCache.INSTANCE.remove( photo.getImage().getURL( Image.LARGE_SQUARE ) );
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        if ( mPhotos != null && mSet != null ) {
            Intent intent = new Intent( this, PhotosetPagerActivity.class );
            intent.putExtra( PhotosetPagerActivity.PARAM_PHOTOSET, mSet );
            intent.putExtra( PhotosetPagerActivity.PARAM_PHOTO, mPhotos.get( position ) );
            startActivity( intent );
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case PhotosetPhotosLoader.SET_LOAD_ERROR:
                mThrobber.setVisibility( View.GONE );
                ErrorDisplayer.displayToast( msg, this );
                return true;

            case PhotosetPhotosLoader.SET_LOADED:
                mThrobber.setVisibility( View.GONE );
                mPhotos = (Paginated<Photo>) msg.obj;
                mAdapter = new PhotosThumbsAdapter( mPhotos, this );
                mGridView.setAdapter( mAdapter );
                return true;
        }
        return false;
    }

}
