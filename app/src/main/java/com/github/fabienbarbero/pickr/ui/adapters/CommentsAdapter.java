/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.ui.adapters;

import java.text.DateFormat;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.Comment;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.UserPhotoLoader;
import com.github.fabienbarbero.pickr.utils.Toolbox;

public class CommentsAdapter
        extends BaseAdapter
{

    private final List<Comment> mComments;
    private final Context mContext;

    public CommentsAdapter( List<Comment> comments, Context context )
    {
        mComments = comments;
        mContext = context;
    }

    @Override
    public int getCount()
    {
        return mComments.size();
    }

    @Override
    public Comment getItem( int position )
    {
        return mComments.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        Comment comment = mComments.get( position );

        CommentHolder holder;
        if ( convertView == null ) {
            convertView = LayoutInflater.from( mContext ).inflate( R.layout.item_comment, parent, false );

            holder = new CommentHolder();
            holder.userImageView = convertView.findViewById( R.id.commentImageView );
            holder.userNameTextView = convertView.findViewById( R.id.commentatorNameTextView );
            holder.commentTextView = convertView.findViewById( R.id.commentTextView );
            holder.dateTextView = convertView.findViewById( R.id.commentDateTextView );
//            holder.task = new UserPhotoLoader( holder.userImageView, comment.getCommentator() );
            convertView.setTag( holder );

        } else {
            holder = (CommentHolder) convertView.getTag();
//            holder.task.cancel();
//            holder.task = new UserPhotoLoader( holder.userImageView, comment.getCommentator() );
        }

        holder.userImageView.setImageResource( R.drawable.ic_user );
        holder.userNameTextView.setText( comment.getCommentator().getName() );
        holder.commentTextView.setText( Html.fromHtml( comment.getValue() ) );
        holder.dateTextView.setText( DateFormat.getDateInstance( DateFormat.MEDIUM ).format( comment.getCreationDate() ) );
        new UserPhotoLoader( holder.userImageView, comment.getCommentator() ).execute();
//        holder.task.execute();

        return convertView;
    }

    private class CommentHolder
    {

        private TextView dateTextView;
        private TextView commentTextView;
        private TextView userNameTextView;
        private ImageView userImageView;
//        private UserPhotoLoader task;

    }

}
