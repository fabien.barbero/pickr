package com.github.fabienbarbero.pickr.tasks;

import android.os.Handler;
import android.os.Message;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.FlickrException;
import com.github.fabienbarbero.flickr.api.entities.UploadedPhoto;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.utils.Logger;

import java.io.File;

public class PhotoUploader
        extends BasicTask<Void>
{

    private static final Logger LOGGER = Logger.getLogger( PhotoUploader.class );
    public static final int PHOTO_ADDED = 45;

    private final File mPhotoFile;
    private final Handler mHandler;

    public PhotoUploader( File photoFile, Handler handler )
    {
        mPhotoFile = photoFile;
        mHandler = handler;
    }

    @Override
    protected Void doInBackground()
    {
        try {
            Flickr flickr = FlickrHandler.getFlickr();
            UploadedPhoto photo = flickr.getUploadService().uploadPhoto( mPhotoFile, mPhotoFile.getName(), null );

            Message msg = mHandler.obtainMessage( PHOTO_ADDED, photo );
            mHandler.sendMessage( msg );

        } catch ( FlickrException ex ) {
            LOGGER.error( "Error loading photo", ex );
        }
        return null;
    }

}
