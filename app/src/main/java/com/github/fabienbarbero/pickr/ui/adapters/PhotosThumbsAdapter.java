/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.PhotoThumbLoader;


public class PhotosThumbsAdapter
        extends PaginatedAdapter<Photo>
{

    private final Context mContext;

    public PhotosThumbsAdapter( Paginated<Photo> photos, Context context )
    {
        super( photos );
        mContext = context;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        ViewHolder holder;
        if ( convertView == null ) {
            convertView = LayoutInflater.from( mContext ).inflate( R.layout.item_photo, null );

            holder = new ViewHolder();
            holder.mImageView = convertView.findViewById( R.id.photoImageView );
            holder.mDescTextView = convertView.findViewById( R.id.photoDescTextView );
            convertView.setTag( holder );

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mImageView.setImageDrawable( null );
        Photo photo = mValues.get( position );
        holder.mDescTextView.setText( photo.getTitle() );
        new PhotoThumbLoader( holder.mImageView, photo ).execute();

        return convertView;
    }

    private static class ViewHolder
    {

        private ImageView mImageView;
        private TextView mDescTextView;
    }

}
