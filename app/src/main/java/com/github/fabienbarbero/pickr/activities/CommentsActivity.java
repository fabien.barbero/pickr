package com.github.fabienbarbero.pickr.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.github.fabienbarbero.flickr.api.entities.Comment;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.PhotoCommentsLoader;
import com.github.fabienbarbero.pickr.ui.adapters.CommentsAdapter;

import java.util.List;

/**
 *
 */

public class CommentsActivity
        extends AppCompatActivity
        implements Handler.Callback
{

    public static final String PARAM_PHOTO = "photo";

    private Handler mHandler;
    private ListView mCommentsListView;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        mHandler = new Handler( this );

        setContentView( R.layout.activity_comments );

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled( true );

        mCommentsListView = findViewById( R.id.commentsListView );

        Photo photo = (Photo) getIntent().getSerializableExtra( PARAM_PHOTO );
        new PhotoCommentsLoader( photo, mHandler ).execute();
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case PhotoCommentsLoader.COMMENTS_LOADED:
                List<Comment> comments = (List<Comment>) msg.obj;
                mCommentsListView.setAdapter( new CommentsAdapter( comments, this ) );
                return true;

            case PhotoCommentsLoader.COMMENTS_LOAD_ERROR:
                // TODO
                return true;
        }
        return false;
    }

}
