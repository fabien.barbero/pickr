/*
 * Copyright (C) 2013 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.github.fabienbarbero.flickr.api.entities.BaseUser;
import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photoset;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.PhotosetsLoader;
import com.github.fabienbarbero.pickr.ui.adapters.PhotosetThumbsAdapter;


public class PhotosetsFragment
        extends FlickrFragment
        implements AdapterView.OnItemClickListener, Callback
{

    private final Handler handler;

    private PhotosetThumbsAdapter mAdapter;
    private OnPhotosetListener mListener;
    private Paginated<Photoset> mSets;

    private GridView mGridView;
    private View mThrobber;

    public PhotosetsFragment()
    {
        super( R.string.title_albums );
        handler = new Handler( this );
    }

    @Override
    public void onAttach( Context context )
    {
        super.onAttach( context );
        mListener = (OnPhotosetListener) context;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_photosets_thumbs_grid, container, false );
        mGridView = view.findViewById( R.id.photosetThumbsGridView );
        mThrobber = view.findViewById( R.id.photosetThumbsGridProgressBar );
        mGridView.setOnItemClickListener( this );
        return view;
    }

    @Override
    public void searchQuery( String query )
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void onResume()
    {
        super.onResume();
        BaseUser user = (BaseUser) getArguments().getSerializable( Constants.PARAM_USER );
        mThrobber.setVisibility( View.VISIBLE );
        new PhotosetsLoader( user, handler ).execute();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        mAdapter.clear();
        mAdapter = null;

        if ( mSets != null ) {
            for ( Photoset set : mSets.asList() ) {
                BitmapCache.INSTANCE.remove( set.getPrimaryPhoto().getURL( Image.LARGE_SQUARE ) );
            }
        }
        mGridView.setAdapter( null );
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        if ( mSets != null && mListener != null ) {
            mListener.onPhotosetSelected( mSets.get( position ) );
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case PhotosetsLoader.SETS_LOAD_ERROR:
                mThrobber.setVisibility( View.GONE );
                ErrorDisplayer.displayToast( msg, getActivity() );
                return true;

            case PhotosetsLoader.SETS_LOADED:
                mThrobber.setVisibility( View.GONE );
                mSets = (Paginated<Photoset>) msg.obj;
                mAdapter = new PhotosetThumbsAdapter( mSets, getActivity() );
                mGridView.setAdapter( mAdapter );
                return true;
        }
        return false;
    }

    public interface OnPhotosetListener
    {

        void onPhotosetSelected( Photoset set );
    }

}
