/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.BaseUser;
import com.github.fabienbarbero.flickr.api.entities.Contact;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.Photoset;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.fragments.ContactsFragment;
import com.github.fabienbarbero.pickr.fragments.FavoritesFragment;
import com.github.fabienbarbero.pickr.fragments.FlickrFragment;
import com.github.fabienbarbero.pickr.fragments.GalleryFragment;
import com.github.fabienbarbero.pickr.fragments.PhotosetsFragment;
import com.github.fabienbarbero.pickr.tasks.UserPhotoLoader;

public class FlickrActivity
        extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   PhotosetsFragment.OnPhotosetListener,
                   GalleryFragment.OnGalleryPhotoListener,
                   FavoritesFragment.OnFavoritePhotoListener,
                   ContactsFragment.OnContactsListener,
                   SearchView.OnQueryTextListener
{

    private DrawerLayout mDrawerLayout;
    private FlickrFragment mCurrentFragment;
    private BaseUser mUser;

    private final FlickrFragment mGalleryFragment = new GalleryFragment();
    private final FlickrFragment mPhotosetsFragment = new PhotosetsFragment();
    private final FlickrFragment mFavoritesFragment = new FavoritesFragment();
    private final FlickrFragment mContactsFragment = new ContactsFragment();

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_flickr );

        // configure the action bar
        Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled( true );
        actionBar.setHomeAsUpIndicator( R.drawable.ic_menu );
        actionBar.setHomeButtonEnabled( true );

        mDrawerLayout = findViewById( R.id.flickrDrawerLayout );
        NavigationView navigationView = findViewById( R.id.flickrNavView );
        navigationView.setCheckedItem( R.id.nav_gallery );
        navigationView.setNavigationItemSelectedListener( this );

        mUser = (BaseUser) getIntent().getSerializableExtra( Constants.PARAM_USER );
        boolean isMe = getIntent().getBooleanExtra( Constants.PARAM_IS_ME, false );

        // Configure navigation header
        View headerLayout = navigationView.getHeaderView( 0);
        TextView userNameTextView = headerLayout.findViewById( R.id.userNameTextView );
        ImageView userInfoImageView = headerLayout.findViewById( R.id.userAvatarImageView );
        userNameTextView.setText( mUser.getName() );
        new UserPhotoLoader( userInfoImageView, mUser ).execute();

        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putSerializable( Constants.PARAM_USER, mUser );
        fragmentBundle.putBoolean( Constants.PARAM_IS_ME, isMe );
        mGalleryFragment.setArguments( fragmentBundle );
        mPhotosetsFragment.setArguments( fragmentBundle );
        mFavoritesFragment.setArguments( fragmentBundle );
        mContactsFragment.setArguments( fragmentBundle );

        // Show first fragment
        showFragment( mGalleryFragment );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_flickr, menu );

        SearchView searchView = (SearchView) menu.findItem( R.id.searchField ).getActionView();
        searchView.setOnQueryTextListener( this );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                mDrawerLayout.openDrawer( GravityCompat.START );
                return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public boolean onQueryTextChange( String newText )
    {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit( String query )
    {
        if ( mCurrentFragment == null ) {
            return false;
        }
        mCurrentFragment.searchQuery( query );
        return true;
    }

    /**
     * Show a fragment in the main view
     *
     * @param fragment The fragment to display
     */
    private void showFragment( FlickrFragment fragment )
    {
        if ( fragment.equals( mCurrentFragment ) ) {
            // Already displayed
            return;
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace( R.id.flickrFragmentsLayout, fragment, fragment.getClass().getSimpleName() );
        transaction.commit();

        getSupportActionBar().setTitle( fragment.getTitle() );

        mCurrentFragment = fragment;
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem menuItem )
    {
        // set item as selected to persist highlight
        menuItem.setChecked( true );

        // close drawer when item is tapped
        mDrawerLayout.closeDrawers();

        switch ( menuItem.getItemId() ) {
            case R.id.nav_gallery:
                showFragment( mGalleryFragment );
                break;
            case R.id.nav_albums:
                showFragment( mPhotosetsFragment );
                break;
            case R.id.nav_favorites:
                showFragment( mFavoritesFragment );
                break;
            case R.id.nav_contacts:
                showFragment( mContactsFragment );
                break;
        }

        return true;
    }

    @Override
    public void onPhotosetSelected( Photoset set )
    {
        Intent intent = new Intent( this, PhotosetActivity.class );
        intent.putExtra( PhotosetActivity.PARAM_SET, set );
        startActivity( intent );
    }

    @Override
    public void onGalleryPhotoSelected( Photo photo )
    {
        Intent intent = new Intent( this, GalleryPagerActivity.class );
        intent.putExtra( GalleryPagerActivity.PARAM_PHOTO, photo );
        intent.putExtra( GalleryPagerActivity.PARAM_USER, mUser );
        startActivity( intent );
    }

    @Override
    public void onFavoritePhotoSelected( Photo photo )
    {
        Intent intent = new Intent( this, FavoritesPagerActivity.class );
        intent.putExtra( FavoritesPagerActivity.PARAM_PHOTO, photo );
        intent.putExtra( FavoritesPagerActivity.PARAM_USER, mUser );
        startActivity( intent );
    }

    @Override
    public void onContactSelected( Contact contact )
    {
        Intent intent = new Intent( this, FlickrActivity.class );
        intent.putExtra( Constants.PARAM_USER, contact );
        startActivity( intent );
    }

}
