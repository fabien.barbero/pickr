package com.github.fabienbarbero.pickr.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.UserInfo;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.AccessTokenVerifier;
import com.github.fabienbarbero.pickr.ui.FinishActivityCallback;
import com.github.fabienbarbero.pickr.utils.Logger;

/**
 *
 */

public class LoginActivity
        extends Activity
        implements Handler.Callback
{

    private static final Logger LOGGER = Logger.getLogger( LoginActivity.class );

    private WebView mWebView;
    private Flickr mFlickr;
    private Handler mHandler;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );

        mFlickr = FlickrHandler.getFlickr();
        mHandler = new Handler( this );

        mWebView = new WebView( this );
        mWebView.setWebViewClient( new OAuthInterceptor() );

        String uri = getIntent().getStringExtra( "uri" );
        setContentView( mWebView );
        mWebView.loadUrl( uri );
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case AccessTokenVerifier.ACCESS_TOKEN_VERIFICATION_ERROR:
                ErrorDisplayer.displayAlert( msg, this, new FinishActivityCallback( this ) );
                return true;

            case AccessTokenVerifier.ACCESS_TOKEN_VERIFIED:
                UserInfo user = (UserInfo) msg.obj;

                Intent intent = new Intent( this, FlickrActivity.class );
                intent.putExtra( Constants.PARAM_USER, user );
                intent.putExtra( Constants.PARAM_IS_ME, true );
                startActivity( intent );
                finish();

                return true;
        }
        return false;
    }

    private class OAuthInterceptor
            extends WebViewClient
    {

        @Override
        public void onPageStarted( WebView view, String url, Bitmap favicon )
        {
            if ( url.startsWith( FlickrHandler.CALLBACK_URI ) ) {
                Uri uri = Uri.parse( url );
                new AccessTokenVerifier( mHandler, mFlickr, uri ).execute();
            }
            super.onPageStarted( view, url, favicon );
        }

    }

}
