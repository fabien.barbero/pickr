/*
 * Copyright (C) 2013 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.flickr.api.entities.Photoset;
import com.github.fabienbarbero.pickr.cache.BitmapCache;


public class PhotosetThumbLoader
        extends BasicTask<Bitmap>
{

    private final ImageView mImageView;
    private final Photoset mSet;

    public PhotosetThumbLoader( ImageView imageView, Photoset set )
    {
        mImageView = imageView;
        mSet = set;
    }

    @Override
    protected Bitmap doInBackground()
    {
        return BitmapCache.INSTANCE.get( mSet.getPrimaryPhoto().getURL( Image.SMALL_320 ) );
    }

    @Override
    protected void onPostExecute( Bitmap result )
    {
        mImageView.setImageBitmap( result );
    }

}
