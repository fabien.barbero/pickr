/*
 * Copyright (C) 2013 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import android.net.Uri;
import android.os.Handler;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.UserInfo;
import com.github.scribejava.core.model.OAuthConstants;


public class AccessTokenVerifier
        extends HandlerTask<UserInfo>
{

    public static final int ACCESS_TOKEN_VERIFIED = 1;
    public static final int ACCESS_TOKEN_VERIFICATION_ERROR = 2;
    //
    private Uri mUri;
    private final Flickr mFlickr;

    public AccessTokenVerifier( Handler handler, Flickr flickr, Uri uri )
    {
        super( handler, ACCESS_TOKEN_VERIFIED, ACCESS_TOKEN_VERIFICATION_ERROR );
        mFlickr = flickr;
        mUri = uri;
    }

    @Override
    protected UserInfo run()
            throws Exception
    {
        String verifier = mUri.getQueryParameter( OAuthConstants.VERIFIER );
        String token = mUri.getQueryParameter( OAuthConstants.TOKEN );

        if ( verifier == null ) {
            throw new IllegalArgumentException( "No authentication code found" );
        }
        if ( token == null ) {
            throw new IllegalArgumentException( "No token code found" );
        }

        mFlickr.verifyToken( verifier, token );
        return mFlickr.getUser();
    }

}
