package com.github.fabienbarbero.pickr.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.UserInfo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.AccessTokenVerifier;
import com.github.fabienbarbero.pickr.tasks.AskAuthorizationTask;
import com.github.fabienbarbero.pickr.tasks.AuthenticationTask;
import com.github.fabienbarbero.pickr.ui.FinishActivityCallback;
import com.github.fabienbarbero.pickr.utils.Logger;


public class BootstrapActivity
        extends Activity
        implements Handler.Callback
{

    private static final Logger LOGGER = Logger.getLogger( BootstrapActivity.class );

    private Handler mHandler;
    private Flickr mFlickr;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.activity_bootstrap );

        mFlickr = FlickrHandler.getFlickr();
        mHandler = new Handler( this );
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if ( mFlickr.isFirstStart() ) {
            LOGGER.debug( "Not logged" );
            new AskAuthorizationTask( mHandler, mFlickr ).execute();

        } else {
            LOGGER.debug( "Authentication" );
            new AuthenticationTask( mHandler, mFlickr ).execute();
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case AuthenticationTask.USER_AUTHENTICATION_ERROR:
            case AccessTokenVerifier.ACCESS_TOKEN_VERIFICATION_ERROR:
            case AskAuthorizationTask.AUTHORIZATION_ASK_ERROR:
                ErrorDisplayer.displayAlert( msg, this, new FinishActivityCallback( this ) );
                return true;

            case AskAuthorizationTask.AUTHORIZATION_ASKED:
                Uri uri = (Uri) msg.obj;
                Intent intent = new Intent( this, LoginActivity.class );
                intent.putExtra( "uri", uri.toString() );
                startActivity( intent );
                return true;

            case AccessTokenVerifier.ACCESS_TOKEN_VERIFIED:
            case AuthenticationTask.USER_AUTHENTICATED:
                UserInfo user = (UserInfo) msg.obj;

                intent = new Intent( this, FlickrActivity.class );
                intent.putExtra( Constants.PARAM_USER, user );
                intent.putExtra( Constants.PARAM_IS_ME, true );
                startActivity( intent );
                finish();

                return true;
        }
        return false;
    }

}
