/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.error;

import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.ui.DialogCallback;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Message;
import android.widget.Toast;

public class ErrorDisplayer
{

    private ErrorDisplayer()
    {

    }

    public static void displayToast( Message msg, Context context )
    {
        if ( msg.obj != null ) {

            if ( msg.obj instanceof LocalizedException ) {
                LocalizedException ex = (LocalizedException) msg.obj;
                Toast.makeText( context, ex.resId(), Toast.LENGTH_LONG ).show();

            } else if ( msg.obj instanceof Throwable ) {
                Throwable th = (Throwable) msg.obj;
                Toast.makeText( context, th.getLocalizedMessage(), Toast.LENGTH_LONG ).show();
            }
        }
    }

    public static void displayAlert( Message msg, Context context, final DialogCallback callback )
    {
        if ( msg.obj != null ) {

            AlertDialog.Builder builder = new AlertDialog.Builder( context );
            builder.setIcon( android.R.drawable.ic_dialog_alert );
            builder.setTitle( R.string.app_name );
            builder.setPositiveButton( R.string.button_ok, new OnClickListener()
            {

                @Override
                public void onClick( DialogInterface dialog, int which )
                {
                    callback.onValidateClick();
                }
            } );

            if ( msg.obj instanceof LocalizedException ) {
                LocalizedException ex = (LocalizedException) msg.obj;
                builder.setMessage( ex.resId() );
                builder.show();

            } else if ( msg.obj instanceof Throwable ) {
                Throwable th = (Throwable) msg.obj;
                builder.setMessage( th.getLocalizedMessage() );
                builder.show();
            }
        }
    }

}
