/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.view.Display;
import android.view.WindowManager;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.PhotoInfo;
import com.github.fabienbarbero.flickr.api.entities.PhotoSize;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.cache.PhotoInfoCache;
import com.github.fabienbarbero.pickr.error.LocalizedException;
import com.github.fabienbarbero.pickr.utils.BitmapUtils;
import com.github.fabienbarbero.pickr.utils.Logger;
import com.github.fabienbarbero.pickr.utils.PhotoSizeWeight;
import com.github.fabienbarbero.pickr.utils.Toolbox;


public class SavePhotoTask
        extends HandlerTask<Void>
{

    private static final Logger LOGGER = Logger.getLogger( SavePhotoTask.class );

    public static final int PHOTO_SAVED = 55;
    public static final int PHOTO_SAVE_ERROR = 56;
    //
    private final Photo mPhoto;
    private final Context mContext;
    private final boolean mWallpaper;

    public SavePhotoTask( Photo photo, boolean wallpaper, Handler handler, Context context )
    {
        super( handler, PHOTO_SAVED, PHOTO_SAVE_ERROR );
        mPhoto = photo;
        mContext = context;
        mWallpaper = wallpaper;
    }

    @Override
    protected Void run()
            throws Exception
    {
        Flickr flickr = FlickrHandler.getFlickr();

        // Check permission
        PhotoInfo info = PhotoInfoCache.INSTANCE.get( mPhoto );
        if ( !info.getUsage().canDownload() ) {
            throw new LocalizedException( "The photo cannot be saved", R.string.error_photo_download_not_allowed );
        }

        File dir = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
        dir.mkdirs();
        File outFile = new File( dir, mPhoto.getId() + ".jpg" );

        List<PhotoSize> sizes = flickr.getPhotosService().getSizes( mPhoto );

        PhotoSize size = PhotoSizeWeight.getOptimalPhotoSize( sizes, mContext );

        OutputStream os = null;
        InputStream is = null;
        try {
            is = size.getSource().openStream();
            os = new FileOutputStream( outFile );
            Toolbox.copyStream( is, os, 131072 );
            os.flush();
            LOGGER.debug( "Photo saved to " + outFile );

        } finally {
            Toolbox.closeQuietly( os );
            Toolbox.closeQuietly( is );
        }

        if ( mWallpaper ) {
            WallpaperManager wallpaperManager = WallpaperManager.getInstance( mContext );
            WindowManager wm = (WindowManager) mContext.getSystemService( Context.WINDOW_SERVICE );
            Display display = wm.getDefaultDisplay();

            Bitmap source = BitmapUtils.fromURL( size.getSource() );

            Bitmap bitmap = Bitmap.createScaledBitmap( source, display.getWidth(), display.getHeight(), true );
            source.recycle();

            wallpaperManager.setBitmap( bitmap );
        }

        return null;
    }

}
