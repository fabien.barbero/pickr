/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.utils;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

import com.github.fabienbarbero.flickr.api.entities.PhotoSize;


public class PhotoSizeWeight
        implements Comparable<PhotoSizeWeight>
{

    public static PhotoSize getOptimalPhotoSize( List<PhotoSize> sizes, Context context )
    {
        WindowManager wm = (WindowManager) context.getSystemService( Context.WINDOW_SERVICE );
        Display display = wm.getDefaultDisplay();

        int resolution = display.getWidth() * display.getHeight();
        return getOptimalPhotoSize( sizes, resolution );
    }

    public static PhotoSize getOptimalPhotoSize( List<PhotoSize> sizes, int screenResolution )
    {
        SortedSet<PhotoSizeWeight> weights = new TreeSet<PhotoSizeWeight>();
        for ( PhotoSize size : sizes ) {
            weights.add( new PhotoSizeWeight( size, Math.abs( screenResolution - ( size.getWidth() * size.getHeight() ) ) ) );
        }
        return weights.first().size;
    }

    private PhotoSize size;
    private int weight;

    private PhotoSizeWeight( PhotoSize size, int weight )
    {
        this.size = size;
        this.weight = weight;
    }

    public PhotoSize getSize()
    {
        return size;
    }

    @Override
    public int compareTo( PhotoSizeWeight another )
    {
        return weight - another.weight;
    }

}
