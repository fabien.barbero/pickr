/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.Photoset;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.PhotosetPhotosLoader;


public class PhotosetPagerActivity
        extends PhotosPagerActivity
        implements Callback
{

    public static final String PARAM_PHOTOSET = "photoset";
    public static final String PARAM_PHOTO = "photo";

    private Handler mHandler;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        mHandler = new Handler( this );
        Photoset set = (Photoset) getIntent().getSerializableExtra( PARAM_PHOTOSET );
        if ( savedInstanceState == null ) {
            new PhotosetPhotosLoader( set, mHandler ).execute();
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case PhotosetPhotosLoader.SET_LOAD_ERROR:
                ErrorDisplayer.displayToast( msg, this );
                return true;

            case PhotosetPhotosLoader.SET_LOADED:
                Paginated<Photo> photos = (Paginated<Photo>) msg.obj;
                setPhotos( photos );
                showPhoto( (Photo) getIntent().getSerializableExtra( PARAM_PHOTO ) );
                return true;
        }
        return super.handleMessage( msg );
    }

}
