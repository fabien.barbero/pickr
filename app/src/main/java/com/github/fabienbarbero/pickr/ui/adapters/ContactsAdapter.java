/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.Contact;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.UserPhotoLoader;
import com.github.fabienbarbero.pickr.utils.Toolbox;

import java.util.List;


public class ContactsAdapter
        extends BaseAdapter
{

    private final Context mContext;
    private final List<Contact> mContacts;

    public ContactsAdapter( List<Contact> contacts, Context context )
    {
        mContacts = contacts;
        mContext = context;
    }

    @Override
    public int getCount()
    {
        return mContacts.size();
    }

    @Override
    public Object getItem( int position )
    {
        return mContacts.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        ViewHolder holder;
        if ( convertView == null ) {
            convertView = LayoutInflater.from( mContext ).inflate( R.layout.item_contact, parent, false );

            holder = new ViewHolder();
            holder.mImageView = convertView.findViewById( R.id.contactAvatarImageView );
            holder.mNameTextView = convertView.findViewById( R.id.contactNameTextView );
            convertView.setTag( holder );

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Contact contact = mContacts.get( position );
        holder.mNameTextView.setText( contact.getName() );

        holder.mImageView.setImageResource( R.drawable.ic_user );
        new UserPhotoLoader( holder.mImageView, contact ).execute();
        return convertView;
    }

    private static class ViewHolder
    {

        private ImageView mImageView;
        private TextView mNameTextView;
    }

}
