package com.github.fabienbarbero.pickr.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.PhotoTag;
import com.github.fabienbarbero.pickr.R;

import java.util.List;

/**
 *
 */

public class PhotoTagsAdapter
        extends BaseAdapter
{
    private final List<PhotoTag> mTags;
    private final Context mContext;

    public PhotoTagsAdapter( List<PhotoTag> tags, Context context )
    {
        mTags = tags;
        mContext = context;
    }

    @Override
    public int getCount()
    {
        return mTags.size();
    }

    @Override
    public Object getItem( int position )
    {
        return mTags.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        ViewHolder holder;
        if ( convertView == null ) {
            convertView = LayoutInflater.from( mContext ).inflate( R.layout.item_tag, parent, false );

            holder = new ViewHolder();
            holder.mTagTextView = convertView.findViewById( R.id.tagTextView );
            convertView.setTag( holder );

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PhotoTag tag = mTags.get( position );
        holder.mTagTextView.setText( tag.getTag() );

        return convertView;
    }

    private static class ViewHolder
    {
        private TextView mTagTextView;
    }

}
