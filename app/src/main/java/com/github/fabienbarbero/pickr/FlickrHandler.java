/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr;

import android.content.Context;

import com.github.fabienbarbero.flickr.api.Flickr;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FlickrHandler
{

    public static final String CALLBACK_URI = "http://pickr.com";

    private static FlickrHandler sInstance;

    public static void init( Context context )
    {
        sInstance = new FlickrHandler( context );
    }

    public static Flickr getFlickr()
    {
        return sInstance.mFlickr;
    }

    private final Flickr mFlickr;

    private FlickrHandler( Context context )
    {
        try ( InputStream is = context.getAssets().open( "pickr.conf" ) ) {
            Properties props = new Properties();
            props.load( is );

            mFlickr = new Flickr( props.getProperty( "apikey" ), props.getProperty( "secret" ),
                                  CALLBACK_URI, "delete",
                                  new File( context.getFilesDir(), "flickr.conf" ) );

        } catch ( IOException ex ) {
            throw new IllegalStateException( "Error loading configuration", ex );
        }
    }
}
