/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Toolbox
{

    private Toolbox()
    {
    }

    public static boolean isStringEmpty( CharSequence seq )
    {
        return seq == null || seq.length() == 0;
    }

    public static CharSequence replaceString( CharSequence template,
                                              Map<String, String> replacementMap )
    {
        // FIXME: dirty ; use regexp
        String stringTemplate = template.toString();
        for ( Map.Entry<String, String> entry : replacementMap.entrySet() ) {
            stringTemplate = stringTemplate.replace( "${" + entry.getKey() + "}", entry.getValue() );
        }
        return stringTemplate;
    }

    public static void closeQuietly( Closeable closeable )
    {
        if ( closeable != null ) {
            try {
                closeable.close();
            } catch ( IOException e ) {
            }
        }
    }

    public static void copyStream( InputStream is, OutputStream os, int bufferSize )
            throws IOException
    {
        byte[] buffer = new byte[bufferSize];
        int len;

        while ( ( len = is.read( buffer ) ) > 0 ) {
            os.write( buffer, 0, len );
        }
    }

    public static String getFinalUrl( String url )
            throws IOException
    {
        HttpURLConnection conn = (HttpURLConnection) new URL( url ).openConnection();
        conn.setConnectTimeout( 5000 );
        conn.setReadTimeout( 15000 );
        try {
            conn.connect();
            String location = conn.getHeaderField( "Location" );

            if ( isStringEmpty( location ) ) {
                return url;
            }
            return location;

        } finally {
            conn.disconnect();
        }
    }

}
