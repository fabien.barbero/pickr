package com.github.fabienbarbero.pickr.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.ExifInfo;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.PhotoInfo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.tasks.ExifLoader;
import com.github.fabienbarbero.pickr.tasks.PhotoInfoLoader;
import com.github.fabienbarbero.pickr.tasks.UserPhotoLoader;
import com.github.fabienbarbero.pickr.utils.Toolbox;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.util.Locale;

/**
 *
 */
public class PhotoInfoActivity
        extends AppCompatActivity
        implements Handler.Callback
{

    public static final String PARAM_PHOTO = "photo";

    private Handler mHandler;

    private TextView photoDescTextView;
    private TextView photoDateTextView;
    private TextView photoViewCountTextView;
//    private LinearLayout photoTagsView;
    private ImageView photoOwnerImageView;
    private TextView photoOwnerTextView;
    private TextView photoCameraTextView;
    private MapView photoLocationMapView;
    private TextView photoLocationTextView;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        mHandler = new Handler( this );

        setContentView( R.layout.activity_photo_info );

        photoDescTextView = findViewById( R.id.photoDescTextView );
        photoDateTextView = findViewById( R.id.photoDateTextView );
        photoViewCountTextView = findViewById( R.id.photoViewCountTextView );
//        photoTagsView = findViewById( R.id.photoTagsLayout );
        photoOwnerImageView = findViewById( R.id.photoOwnerImageView );
        photoOwnerTextView = findViewById( R.id.photoOwnerTextView );
        photoCameraTextView = findViewById( R.id.cameraInfoTextView );
        photoLocationTextView = findViewById( R.id.photoLocationTextView );
        photoLocationMapView = findViewById( R.id.photoLocationMapView );

        photoLocationMapView.onCreate( savedInstanceState );

        Photo photo = (Photo) getIntent().getSerializableExtra( PARAM_PHOTO );

        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( getResources().getColor( R.color.primary_color ) ) );
        getSupportActionBar().setTitle( photo.getTitle() );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        new PhotoInfoLoader( mHandler, photo ).execute();
        new ExifLoader( photo, mHandler ).execute();
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public void onResume()
    {
        photoLocationMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        photoLocationMapView.onDestroy();
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        photoLocationMapView.onLowMemory();
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case ExifLoader.EXIF_LOAD_SUCCESS:
                ExifInfo exifInfo = (ExifInfo) msg.obj;
                photoCameraTextView.setText( exifInfo.getCamera() );
                return true;


            case PhotoInfoLoader.PHOTO_INFO_LOADED:
                final PhotoInfo photoInfo = (PhotoInfo) msg.obj;

                if ( !Toolbox.isStringEmpty( photoInfo.getDescription().trim() ) ) {
                    photoDescTextView.setText( photoInfo.getDescription() );
                }
                DateFormat df = DateFormat.getDateInstance( DateFormat.LONG, Locale.getDefault() );
                photoDateTextView.setText( df.format( photoInfo.getDates().getTakenDate() ) );
                photoViewCountTextView.setText( String.valueOf( photoInfo.getViews() ) );

//                if ( photoInfo.getTags().isEmpty() ) {
//                    photoTagsView.setVisibility( View.VISIBLE );
//                    photoTagsView.removeAllViews();
//                    LayoutInflater inflater = LayoutInflater.from( this );
//                    for ( PhotoTag tag : photoInfo.getTags() ) {
//                        View tagView = inflater.inflate( R.layout.item_tag, photoTagsView, false );
//                        TextView tagTextView = tagView.findViewById( R.id.tagTextView );
//                        tagTextView.setText( tag.getTag() );
//                    }
//                }

                photoOwnerTextView.setText( photoInfo.getOwner().getName() );
                new UserPhotoLoader( photoOwnerImageView, photoInfo.getOwner() ).execute();

                if ( photoInfo.getLocation() != null ) {
                    photoLocationTextView.setText( String.format( "%s - %s", photoInfo.getLocation().getLocality(), photoInfo.getLocation().getCountry() ) );
                    photoLocationMapView.setVisibility( View.VISIBLE );
                    photoLocationMapView.getMapAsync( new OnMapReadyCallback()
                    {

                        @Override
                        public void onMapReady( GoogleMap googleMap )
                        {
                            LatLng location = new LatLng( photoInfo.getLocation().getLatitude(),
                                                          photoInfo.getLocation().getLongitude() );
                            googleMap.addMarker( new MarkerOptions().position( location ).title( photoInfo.getTitle() ) );
                            googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom( location, 10 ) );
                        }

                    } );
                }
        }
        return false;
    }

}
