package com.github.fabienbarbero.pickr.tasks;

import android.os.Handler;

import com.github.fabienbarbero.flickr.api.entities.ExifInfo;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.FlickrHandler;

/**
 *
 */
public class ExifLoader
        extends HandlerTask<ExifInfo>
{

    public static final int EXIF_LOAD_SUCCESS = 26;
    public static final int EXIF_LOAD_ERROR = 27;

    private final Photo mPhoto;

    public ExifLoader( Photo photo, Handler handler )
    {
        super( handler, EXIF_LOAD_SUCCESS, EXIF_LOAD_ERROR );
        mPhoto = photo;
    }

    @Override
    protected ExifInfo run()
            throws Exception
    {
        return FlickrHandler.getFlickr().getPhotosService().getExif( mPhoto );
    }
}
