/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.utils;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import com.github.fabienbarbero.flickr.api.utils.IOUtils;

public class BitmapUtils
{

    private static final Logger LOGGER = Logger.getLogger( BitmapUtils.class );

    private BitmapUtils()
    {
    }

    public static Bitmap fromURL( URL url )
    {
        try {
            return loadBitmap( url );

        } catch ( FileNotFoundException ex ) {
            return null; // 404

        } catch ( IOException ex ) {
            // Retry one more time
            try {
                return loadBitmap( url );
            } catch ( IOException ex1 ) {
                LOGGER.error( "Error loading image " + url, ex1 );
                return null;
            }
        }
    }

    private static Bitmap loadBitmap( URL url )
            throws IOException
    {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout( 5000 );
        conn.setReadTimeout( 15000 );
        conn.connect();

        try ( InputStream is = new BufferedInputStream( conn.getInputStream(), 512000 ) ) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            return BitmapFactory.decodeStream( is, null, options );

        } finally {
            conn.disconnect();
        }
    }

    public static int calculateInSampleSize( int width, int height, int reqWidth, int reqHeight )
    {
        int inSampleSize = 1;

        if ( height > reqHeight || width > reqWidth ) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ( ( halfHeight / inSampleSize ) > reqHeight && ( halfWidth / inSampleSize ) > reqWidth ) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap toGrayscale( Bitmap bmpOriginal )
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap( width, height, Bitmap.Config.RGB_565 );
        Canvas c = new Canvas( bmpGrayscale );
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation( 0 );
        ColorMatrixColorFilter f = new ColorMatrixColorFilter( cm );
        paint.setColorFilter( f );
        c.drawBitmap( bmpOriginal, 0, 0, paint );
        return bmpGrayscale;
    }

}
