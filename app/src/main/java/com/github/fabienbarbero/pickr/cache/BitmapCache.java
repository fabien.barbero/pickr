/*
 * Copyright (C) 2014 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.cache;

import java.net.URL;

import com.github.fabienbarbero.pickr.BuildConfig;
import com.github.fabienbarbero.pickr.utils.BitmapUtils;
import com.github.fabienbarbero.pickr.utils.Logger;

import android.graphics.Bitmap;
import android.util.LruCache;


public class BitmapCache
        extends LruCache<URL, Bitmap>
{

    private static final Logger LOGGER = Logger.getLogger( BitmapCache.class );
    public static final LruCache<URL, Bitmap> INSTANCE = new BitmapCache();

    private BitmapCache()
    {
        super( 104857600 ); // 100MB
    }

    public void free( Bitmap bitmap )
    {
        bitmap.recycle();
    }

    @Override
    protected Bitmap create( URL key )
    {
        return BitmapUtils.fromURL( key );
    }

    @Override
    protected int sizeOf( URL key, Bitmap value )
    {
        return value.getByteCount();
    }

    @Override
    protected void entryRemoved( boolean evicted, URL key, Bitmap oldValue, Bitmap newValue )
    {
        if ( !evicted ) {
            oldValue.recycle();
            if ( BuildConfig.DEBUG ) {
                LOGGER.debug( "Bitmap recycled" );
            }
        }
    }

}
