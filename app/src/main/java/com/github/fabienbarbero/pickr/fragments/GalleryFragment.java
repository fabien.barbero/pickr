/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.fabienbarbero.flickr.api.entities.BaseUser;
import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.GalleryLoader;
import com.github.fabienbarbero.pickr.ui.adapters.PhotosThumbsAdapter;

public class GalleryFragment
        extends ThumbsGridFragment
        implements Callback
{

    private OnGalleryPhotoListener mListener;
    private PhotosThumbsAdapter mAdapter;
    private Paginated<Photo> mPhotos;

    public GalleryFragment()
    {
        super( R.string.title_gallery );
    }

    @Override
    public void onAttach( Context context )
    {
        super.onAttach( context );
        mListener = (OnGalleryPhotoListener) context;
    }

    @Override
    public View onCreateView( LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = super.onCreateView( inflater, container, savedInstanceState );
        reload();
        return view;
    }

    @Override
    protected void reload()
    {
        mThrobber.setVisibility( View.VISIBLE );
        BaseUser user = (BaseUser) getArguments().getSerializable( Constants.PARAM_USER );
        new GalleryLoader( user, mHandler ).execute();
    }

    @Override
    public void searchQuery( String query )
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        if ( mPhotos != null ) {
            for ( Photo photo : mPhotos ) {
                BitmapCache.INSTANCE.remove( photo.getImage().getURL( Image.LARGE_SQUARE ) );
            }
        }
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        if ( mPhotos != null && mListener != null ) {
            mListener.onGalleryPhotoSelected( mPhotos.get( position ) );
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        super.handleMessage( msg );

        switch ( msg.what ) {
            case GalleryLoader.PHOTOS_LOAD_ERROR:
                mThrobber.setVisibility( View.GONE );
                ErrorDisplayer.displayToast( msg, getActivity() );
                return true;

            case GalleryLoader.PHOTOS_LOADED:
                mThrobber.setVisibility( View.GONE );
                mPhotos = (Paginated<Photo>) msg.obj;
                mAdapter = new PhotosThumbsAdapter( mPhotos, getActivity() );
                mGridView.setAdapter( mAdapter );
                mRefreshLayout.setRefreshing( false );
                return true;
        }
        return false;
    }

    public interface OnGalleryPhotoListener
    {

        void onGalleryPhotoSelected( Photo photo );
    }

}
