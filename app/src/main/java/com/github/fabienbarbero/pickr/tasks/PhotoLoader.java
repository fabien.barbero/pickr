/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.flickr.api.entities.PhotoSize;
import com.github.fabienbarbero.flickr.api.utils.IOUtils;
import com.github.fabienbarbero.pickr.BuildConfig;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.utils.BitmapUtils;
import com.github.fabienbarbero.pickr.utils.Logger;
import com.github.fabienbarbero.pickr.utils.PhotoSizeWeight;

public class PhotoLoader
        extends BasicTask<Bitmap>
{

    private static final Logger LOGGER = Logger.getLogger( PhotoLoader.class );
    public static final int PHOTO_URL = 35;

    private final ImageView mImageView;
    private final Photo mPhoto;
    private final Context mContext;
    private final Handler mHandler;

    public PhotoLoader( ImageView imageView, Photo photo, Handler handler, Context context )
    {
        mImageView = imageView;
        mPhoto = photo;
        mHandler = handler;
        mContext = context;
    }

    @Override
    protected Bitmap doInBackground()
    {
        try {
            WindowManager wm = (WindowManager) mContext.getSystemService( Context.WINDOW_SERVICE );
            Display display = wm.getDefaultDisplay();

            int resolution = display.getWidth() * display.getHeight();

            Flickr flickr = FlickrHandler.getFlickr();
            List<PhotoSize> sizes = flickr.getPhotosService().getSizes( mPhoto );

            return loadBitmap( PhotoSizeWeight.getOptimalPhotoSize( sizes, resolution ), display );

        } catch ( Throwable ex ) {
            LOGGER.error( "Error loading photo", ex );
        }
        return null;
    }

    private Bitmap loadBitmap( PhotoSize size, Display display )
            throws IOException
    {
        InputStream is = null;
        try {
            if ( BuildConfig.DEBUG ) {
                LOGGER.debug( "Loading photo with size " + size.getWidth() + "x" + size.getHeight() + " for screen " + display.getWidth()
                                      + "x" + display.getHeight() );
            }
            URL url = size.getSource();
            Bitmap bitmap = BitmapCache.INSTANCE.get( url );

            if ( bitmap == null ) {
                is = url.openStream();

                Options options = new Options();
                options.inPurgeable = true;
                options.inInputShareable = true;
                options.inSampleSize = BitmapUtils.calculateInSampleSize( size.getWidth(), size.getHeight(), display.getWidth(),
                                                                          display.getHeight() );
                bitmap = BitmapFactory.decodeStream( is, null, options );
                BitmapCache.INSTANCE.put( url, bitmap );

            }
            Message msg = mHandler.obtainMessage( PHOTO_URL, url );
            mHandler.sendMessage( msg );
            return bitmap;

        } finally {
            IOUtils.closeQuietly( is );
        }
    }

    @Override
    protected void onPostExecute( Bitmap result )
    {
        mImageView.setImageBitmap( result );
    }

}
