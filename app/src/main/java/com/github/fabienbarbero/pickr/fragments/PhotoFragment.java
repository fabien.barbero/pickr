/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.fragments;

import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.tasks.PhotoLoader;

public class PhotoFragment
        extends Fragment
        implements Callback, OnClickListener
{

    public static final String PARAM_PHOTO = "photo";
    private static final String PARAM_PHOTO_URL = "photoUrl";

    private final Handler mHandler;

    private URL mPhotoUrl;
    private Photo mPhoto;

    private ImageView mImageView;
    private ProgressBar mProgressBar;

    public PhotoFragment()
    {
        super();
        mHandler = new Handler( this );
        setRetainInstance( true );
    }

    public Photo getPhoto()
    {
        if ( mPhoto == null ) {
            mPhoto = (Photo) getArguments().getSerializable( PARAM_PHOTO );
        }
        return mPhoto;
    }

    public Bitmap getBitmap()
    {
        Drawable drawable = mImageView.getDrawable();
        if ( drawable instanceof BitmapDrawable ) {
            return ( (BitmapDrawable) drawable ).getBitmap();
        }
        return null;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_photo, null );
        mImageView = view.findViewById( R.id.photoImageView );
        mProgressBar = view.findViewById( R.id.photoLoadingProgressBar );

        mImageView.setOnClickListener( this );
        mProgressBar.setVisibility( View.VISIBLE );

        if ( savedInstanceState != null ) {
            mPhoto = (Photo) savedInstanceState.getSerializable( PARAM_PHOTO );
            mPhotoUrl = (URL) savedInstanceState.getSerializable( PARAM_PHOTO_URL );
        }

        new PhotoLoader( mImageView, getPhoto(), mHandler, getActivity() ).execute();

        return view;
    }

    @Override
    public void onSaveInstanceState( Bundle outState )
    {
        super.onSaveInstanceState( outState );

        if ( outState != null ) {
            if ( mPhoto != null ) {
                outState.putSerializable( PARAM_PHOTO, mPhoto );
            }
            if ( mPhotoUrl != null ) {
                outState.putSerializable( PARAM_PHOTO_URL, mPhotoUrl );
            }
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        if ( mPhotoUrl != null ) {
            BitmapCache.INSTANCE.remove( mPhotoUrl );
        }

        if ( mImageView != null ) {
            mImageView.setImageDrawable( null );
        }
    }

    @Override
    public void onClick( View view )
    {
        if ( view.getId() == R.id.photoImageView ) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            ActionBar actionBar = activity.getSupportActionBar();
            View toolsView = activity.findViewById( R.id.photosPagerToolsView );

            if ( actionBar.isShowing() ) {
                actionBar.hide();
                toolsView.animate().translationY( toolsView.getHeight() );
            } else {
                actionBar.show();
                toolsView.animate().translationY( 0 );
            }
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {

            case PhotoLoader.PHOTO_URL:
                mPhotoUrl = (URL) msg.obj;
                mProgressBar.setVisibility( View.GONE );
                return true;
        }
        return false;
    }

}
