package com.github.fabienbarbero.pickr.tasks;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.pickr.cache.BitmapCache;

/**
 *
 */

public class HeaderImageLoader
        extends BasicTask<Bitmap>
{

    private final Image image;
    private final ImageView mImageView;

    public HeaderImageLoader( Image image, ImageView mImageView )
    {
        this.image = image;
        this.mImageView = mImageView;
    }

    @Override
    protected Bitmap doInBackground()
    {
        return BitmapCache.INSTANCE.get( image.getURL( Image.MEDIUM_1024 ) );
    }

    @Override
    protected void onPostExecute( Bitmap result )
    {
        mImageView.setImageBitmap( result );
    }

}
