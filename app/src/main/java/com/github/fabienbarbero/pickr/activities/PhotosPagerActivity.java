/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.fabienbarbero.flickr.api.entities.Image;
import com.github.fabienbarbero.flickr.api.entities.Paginated;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.fragments.PhotoFragment;
import com.github.fabienbarbero.pickr.tasks.SavePhotoTask;
import com.github.fabienbarbero.pickr.ui.adapters.PhotosPagerAdapter;

import java.util.LinkedList;
import java.util.List;

public abstract class PhotosPagerActivity
        extends AppCompatActivity
        implements ViewPager.OnPageChangeListener, Callback, View.OnClickListener
{

    private static final int REQUEST_SHARE = 54;

    protected ViewPager mPager;
    private List<PhotoFragment> mFragments;
    private int mStartIndex = -1;
    private Handler mHandler;


    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        mHandler = new Handler( this );
        setContentView( R.layout.activity_photos_pager );

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle( null );
        actionBar.setDisplayHomeAsUpEnabled( true );
        actionBar.setBackgroundDrawable( new ColorDrawable( Color.parseColor( "#99333333" ) ) );

        ImageButton photoInfoButton = findViewById( R.id.photoInfoButton );
        photoInfoButton.setOnClickListener( this );

        ImageButton photoShareButton = findViewById( R.id.photoShareButton );
        photoShareButton.setOnClickListener( this );

        ImageButton photoCommentsButton = findViewById( R.id.photoCommentsButton );
        photoCommentsButton.setOnClickListener( this );

        mPager = findViewById( R.id.photosPager );
        mPager.addOnPageChangeListener( this );
        mPager.setPageMargin( 10 );
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState )
    {
        super.onRestoreInstanceState( savedInstanceState );

        if ( savedInstanceState != null ) {
            mStartIndex = savedInstanceState.getInt( "pager.position" );
        }
    }

    @Override
    protected void onSaveInstanceState( Bundle outState )
    {
        super.onSaveInstanceState( outState );
        if ( outState != null && mPager != null ) {
            outState.putInt( "pager.position", mPager.getCurrentItem() );
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_photo, menu );
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public void onClick( View v )
    {
        switch ( v.getId() ) {
            case R.id.photoInfoButton:
                PhotoFragment fragment = mFragments.get( mPager.getCurrentItem() );
                Intent intent = new Intent( this, PhotoInfoActivity.class );
                intent.putExtra( PhotoInfoActivity.PARAM_PHOTO, fragment.getPhoto() );
                startActivity( intent );
                break;

            case R.id.photoCommentsButton:
                fragment = mFragments.get( mPager.getCurrentItem() );
                intent = new Intent( this, CommentsActivity.class );
                intent.putExtra( CommentsActivity.PARAM_PHOTO, fragment.getPhoto() );
                startActivity( intent );
                break;

            case R.id.photoShareButton:
                fragment = mFragments.get( mPager.getCurrentItem() );
                Uri uri = Uri.parse( fragment.getPhoto().getImage().getURL( Image.ORIGINAL ).toString() );

                Intent shareIntent = new Intent();
                shareIntent.setAction( Intent.ACTION_SEND );
                shareIntent.putExtra( Intent.EXTRA_STREAM, uri );
                shareIntent.setType( "image/jpeg" );
                startActivityForResult( Intent.createChooser( shareIntent, getResources().getText( R.string.send_to ) ),
                                        REQUEST_SHARE );
                break;
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        switch ( requestCode ) {
            case REQUEST_SHARE:

                break;
        }
        super.onActivityResult( requestCode, resultCode, data );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.photoSaveItem:
                new SavePhotoTask( mFragments.get( mPager.getCurrentItem() ).getPhoto(), false, mHandler, this ).execute();
                return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public void onPageScrolled( int position, float positionOffset, int positionOffsetPixels )
    {
    }

    @Override
    public void onPageScrollStateChanged( int state )
    {
    }

    @Override
    public void onPageSelected( int position )
    {
        if ( mFragments != null ) {
            getSupportActionBar().setTitle( mFragments.get( position ).getPhoto().getTitle() );
        }
    }

    protected void showPhoto( Photo photo )
    {
        if ( mFragments != null ) {
            if ( mStartIndex != -1 ) {
                // Dirty!
                mPager.setCurrentItem( mStartIndex );
                mStartIndex = -1;

            } else {
                for ( int i = 0; i < mFragments.size(); i++ ) {
                    if ( mFragments.get( i ).getPhoto().equals( photo ) ) {
                        mPager.setCurrentItem( i );
                        return;
                    }
                }
            }
        }
    }

    protected void setPhotos( Paginated<Photo> photos )
    {
        mFragments = new LinkedList<>();

        PhotoFragment fragment;
        Bundle args;
        for ( Photo photo : photos ) {
            args = new Bundle();
            args.putSerializable( PhotoFragment.PARAM_PHOTO, photo );

            fragment = new PhotoFragment();
            fragment.setArguments( args );
            mFragments.add( fragment );
        }
        mPager.setAdapter( new PhotosPagerAdapter( getSupportFragmentManager(), mFragments ) );
        mPager.setCurrentItem( mStartIndex );
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case SavePhotoTask.PHOTO_SAVED:
                Toast.makeText( this, R.string.photo_saved, Toast.LENGTH_LONG ).show();
                return true;

            case SavePhotoTask.PHOTO_SAVE_ERROR:
                ErrorDisplayer.displayToast( msg, this );
                return true;
        }
        return false;
    }

}
