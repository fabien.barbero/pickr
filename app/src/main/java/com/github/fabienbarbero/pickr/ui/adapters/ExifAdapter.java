/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.fabienbarbero.flickr.api.entities.ExifEntry;
import com.github.fabienbarbero.pickr.R;

import java.util.List;


public class ExifAdapter
        extends BaseAdapter
{

    private final List<ExifEntry> mEntries;
    private final Context mContext;

    public ExifAdapter( List<ExifEntry> entries, Context context )
    {
        mEntries = entries;
        mContext = context;
    }

    @Override
    public int getCount()
    {
        return mEntries.size();
    }

    @Override
    public ExifEntry getItem( int position )
    {
        return mEntries.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public boolean isEnabled( int position )
    {
        return false;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        if ( convertView == null ) {
            convertView = LayoutInflater.from( mContext ).inflate( R.layout.item_exif, parent, false );
        }

        TextView labelTextView = (TextView) convertView.findViewById( R.id.exifLabelTextView );
        TextView valueTextView = (TextView) convertView.findViewById( R.id.exifValueTextView );

        ExifEntry entry = mEntries.get( position );
        labelTextView.setText( entry.getLabel() );
        valueTextView.setText( entry.toString() );

        return convertView;
    }

}
