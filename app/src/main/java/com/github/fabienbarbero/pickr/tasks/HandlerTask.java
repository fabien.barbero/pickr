/*
 * Copyright (C) 2013 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import java.util.concurrent.ExecutorService;

import android.os.Handler;
import android.os.Message;

import com.github.fabienbarbero.pickr.utils.Logger;

public abstract class HandlerTask<T>
{

    private final Handler mHandler;
    private final int mSuccessCode;
    private final int mErrorCode;
    private ExecutorService mExecutor = BasicTask.EXECUTOR_MULTI_THREADS;

    public HandlerTask( Handler handler, int successCode, int errorCode )
    {
        mHandler = handler;
        mSuccessCode = successCode;
        mErrorCode = errorCode;
    }

    public final void execute()
    {
        mExecutor.submit( new Runnable()
        {

            @Override
            public void run()
            {
                try {
                    T result = HandlerTask.this.run();
                    Message msg = mHandler.obtainMessage( mSuccessCode, result );
                    mHandler.sendMessage( msg );

                } catch ( Exception ex ) {
                    Logger.getLogger( getClass() ).error( "Execution error", ex );

                    Message msg = mHandler.obtainMessage( mErrorCode, ex );
                    mHandler.sendMessage( msg );
                }
            }

        } );
    }

    protected abstract T run()
            throws Exception;

}
