/*
 * Copyright (C) 2014 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import android.os.Handler;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.entities.Comment;
import com.github.fabienbarbero.flickr.api.entities.Photo;
import com.github.fabienbarbero.pickr.FlickrHandler;

import java.util.List;


public class PhotoCommentsLoader
        extends HandlerTask<List<Comment>>
{

    public static final int COMMENTS_LOADED = 70;
    public static final int COMMENTS_LOAD_ERROR = 71;

    private final Photo mPhoto;

    public PhotoCommentsLoader( Photo photo, Handler handler )
    {
        super( handler, COMMENTS_LOADED, COMMENTS_LOAD_ERROR );
        mPhoto = photo;
    }

    @Override
    protected List<Comment> run()
            throws Exception
    {
        Flickr flickr = FlickrHandler.getFlickr();
        return flickr.getPhotosService().getComments( mPhoto );
    }

}
