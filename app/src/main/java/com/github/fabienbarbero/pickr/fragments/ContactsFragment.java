/*
 * Copyright (C) 2014 Fabien Barbero
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.github.fabienbarbero.flickr.api.entities.Avatar;
import com.github.fabienbarbero.flickr.api.entities.BaseUser;
import com.github.fabienbarbero.flickr.api.entities.Contact;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.Constants;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.error.ErrorDisplayer;
import com.github.fabienbarbero.pickr.tasks.ContactsLoader;
import com.github.fabienbarbero.pickr.ui.adapters.ContactsAdapter;

import java.util.List;


public class ContactsFragment
        extends FlickrFragment
        implements Callback, OnItemClickListener
{

    private final Handler handler;
    private OnContactsListener mListener;
    private List<Contact> mContacts;
    private ContactsAdapter mAdapter;
    //
    private View mThrobber;
    private GridView mGridView;

    public ContactsFragment()
    {
        super( R.string.title_contacts );
        handler = new Handler( this );
    }

    @Override
    public void onAttach( Context context )
    {
        super.onAttach( context );
        mListener = (OnContactsListener) context;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_contacts_grid, container, false );
        mGridView = view.findViewById( R.id.contactsGridView );
        mThrobber = view.findViewById( R.id.contactsGridProgressBar );

        mGridView.setOnItemClickListener( this );
        BaseUser user = (BaseUser) getArguments().getSerializable( Constants.PARAM_USER );
        new ContactsLoader( user, handler ).execute();
        return view;
    }

    @Override
    public void searchQuery( String query )
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        if ( mContacts != null ) {
            for ( Contact contact : mContacts ) {
                BitmapCache.INSTANCE.remove( contact.getAvatar().getUrl( Avatar.SMALL_SQUARE ) );
                BitmapCache.INSTANCE.remove( contact.getAvatar().getUrl( Avatar.MEDIUM_SQUARE ) );
            }
        }
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        if ( mContacts != null && mListener != null ) {
            mListener.onContactSelected( mContacts.get( position ) );
        }
    }

    @Override
    public boolean handleMessage( Message msg )
    {
        switch ( msg.what ) {
            case ContactsLoader.CONTACT_LOAD_ERROR:
                mThrobber.setVisibility( View.GONE );
                ErrorDisplayer.displayToast( msg, getActivity() );
                return true;

            case ContactsLoader.CONTACT_LOADED:
                mThrobber.setVisibility( View.GONE );
                mContacts = (List<Contact>) msg.obj;
                mAdapter = new ContactsAdapter( mContacts, getActivity() );
                mGridView.setAdapter( mAdapter );
                return true;
        }
        return false;
    }

    public interface OnContactsListener
    {

        void onContactSelected( Contact contact );
    }

}
