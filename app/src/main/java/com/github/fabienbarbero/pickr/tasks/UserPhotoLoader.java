/*
 * Copyright (C) 2014 Fabien Barbero Permission is hereby granted, free of
 * charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.github.fabienbarbero.pickr.tasks;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.widget.ImageView;

import com.github.fabienbarbero.flickr.api.Flickr;
import com.github.fabienbarbero.flickr.api.FlickrException;
import com.github.fabienbarbero.flickr.api.entities.Avatar;
import com.github.fabienbarbero.flickr.api.entities.BaseUser;
import com.github.fabienbarbero.flickr.api.entities.UserInfo;
import com.github.fabienbarbero.pickr.FlickrHandler;
import com.github.fabienbarbero.pickr.R;
import com.github.fabienbarbero.pickr.cache.BitmapCache;
import com.github.fabienbarbero.pickr.utils.Logger;

public class UserPhotoLoader
        extends BasicTask<Bitmap>
{

    private static final Logger LOGGER = Logger.getLogger( UserPhotoLoader.class );
    private final ImageView mImageView;
    private final BaseUser mUser;

    public UserPhotoLoader( ImageView imageView, BaseUser user )
    {
        mImageView = imageView;
        mUser = user;
    }

    @Override
    protected Bitmap doInBackground()
    {
        try {
            Flickr flickr = FlickrHandler.getFlickr();
            UserInfo info = flickr.getPeopleService().getUserInfo( mUser );

            if ( info.getAvatar().isSet() ) {
                Bitmap bitmap = BitmapCache.INSTANCE.get( info.getAvatar().getUrl( Avatar.MEDIUM_SQUARE ) );
                if ( bitmap == null ) {
                    bitmap = BitmapCache.INSTANCE.get( info.getAvatar().getUrl( Avatar.SMALL_SQUARE ) );
                }
                if ( bitmap != null ) {
                    return getCurveImage( bitmap );
                }
            }

        } catch ( FlickrException e ) {
            LOGGER.error( "Error loading user photo", e );
        }
        return null;
    }

    @Override
    protected void onPostExecute( Bitmap avatar )
    {
        if ( avatar != null ) {
            mImageView.setImageBitmap( avatar );
        } else {
            mImageView.setImageResource( R.drawable.ic_user );
        }
    }

    public static Bitmap getCurveImage( Bitmap bitmap )
    {
        int w = bitmap.getWidth(), h = bitmap.getHeight();

        Bitmap rounder = Bitmap.createBitmap( w, h, Bitmap.Config.ARGB_8888 );
        Canvas canvas = new Canvas( rounder );

        Paint xferPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        xferPaint.setColor( Color.RED );

        // canvas.drawRoundRect(new RectF(0, 0, w, h), 5.0f, 5.0f, xferPaint);
        canvas.drawCircle( w / 2, w / 2, w / 2, xferPaint );

        xferPaint.setXfermode( new PorterDuffXfermode( PorterDuff.Mode.DST_IN ) );

        Bitmap result = Bitmap.createBitmap( bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888 );
        Canvas resultCanvas = new Canvas( result );
        resultCanvas.drawBitmap( bitmap, 0, 0, null );
        resultCanvas.drawBitmap( rounder, 0, 0, xferPaint );

        return result;
    }

}
